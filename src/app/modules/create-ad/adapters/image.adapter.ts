import { Injectable } from '@angular/core';
import { Adapter } from '../../../core/models/adapter';
import { Image } from '../models/image.model';
import { Observable } from 'rxjs';

@Injectable()
export class ImageAdapter implements Adapter<Image, File> {
  public adapt(item: any): Observable<Image> {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(item);

    return new Observable<Image>((observer) => {
      fileReader.onloadend = () => {
        observer.next({
          base64: fileReader.result as string,
          name: item.name,
          size: item.size,
          blob: item,
        });
        observer.complete();
      };

      fileReader.onerror = () => {
        observer.error(fileReader.error);
        observer.complete();
      };
    });
  }
}
