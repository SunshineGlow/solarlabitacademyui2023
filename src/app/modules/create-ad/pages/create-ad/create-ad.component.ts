import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UtilsService } from '../../../../core/services/utils.service';

@Component({
  selector: 'app-create-ad',
  templateUrl: './create-ad.component.html',
  styleUrls: ['./create-ad.component.scss'],
})
export class CreateAdComponent {
  public readonly form = this._fb.group({
    category: ['', [Validators.required]],
    title: ['', [Validators.required]],
    description: ['', [Validators.required]],
    address: ['', [Validators.required]],
    images: [[]],
    price: [null, [Validators.required]],
  });

  constructor(
    private readonly _fb: FormBuilder,
    private readonly _utilsService: UtilsService
  ) {}

  submit() {
    console.log(this.form.getRawValue());
    this._utilsService.markAllAsDirty(this.form);
    if (this.form.invalid) {
      return;
    }
  }
}
