import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ImageAdapter } from '../../adapters/image.adapter';
import { Image } from '../../models/image.model';
import { from, mergeMap, Observable, OperatorFunction, switchMap } from 'rxjs';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-images-upload',
  templateUrl: './images-upload.component.html',
  styleUrls: ['./images-upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: ImagesUploadComponent,
    },
    MessageService,
  ],
})
export class ImagesUploadComponent implements ControlValueAccessor {
  @Input() public disabled = false;
  @Input() public maxLength = 10;
  public value: Image[] = [];

  constructor(
    private readonly _cdr: ChangeDetectorRef,
    private readonly _imageAdapter: ImageAdapter,
    private readonly messageService: MessageService
  ) {}

  public onBlur() {
    this.onTouched();
  }

  public onModelChange() {
    this.onChange(this.value);
    this.onTouched();
  }

  public onChange = (value: any) => {};
  public onTouched = () => {};

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this._cdr.detectChanges();
  }

  public writeValue(obj: any): void {
    this.value = obj;
    this._cdr.detectChanges();
  }

  public upload($event: any): void {
    if ($event.target.files.length + this.value.length > this.maxLength) {
      this.messageService.add({
        severity: 'error',
        summary: 'Ошибка',
        detail: `Не более ${this.maxLength} картинок`,
      });
      return;
    }

    from($event.target.files)
      .pipe(mergeMap((file) => this._imageAdapter.adapt(file as File)))
      .subscribe({
        next: (file) => {
          this.value.push(file);
        },
        complete: () => {
          this.writeValue(this.value);
        },
      });
  }

  public removeItem(index: number): void {
    if (index > -1) {
      this.value.splice(index, 1);
      this.writeValue(this.value);
    }
  }
}
