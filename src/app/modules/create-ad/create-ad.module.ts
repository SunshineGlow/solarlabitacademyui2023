import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateAdRoutingModule } from './create-ad-routing.module';
import { CreateAdComponent } from './pages/create-ad/create-ad.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ValidationComponentModule } from '../../shared/components/validation/validation.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';
import { InputNumberModule } from 'primeng/inputnumber';
import { ImagesUploadComponent } from './components/images-upload/images-upload.component';
import { FileUploadModule } from 'primeng/fileupload';
import { ImageAdapter } from './adapters/image.adapter';
import { FileSizePipeModule } from '../../shared/pipes/file-size.pipe';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [CreateAdComponent, ImagesUploadComponent],
  imports: [
    CommonModule,
    CreateAdRoutingModule,
    ReactiveFormsModule,
    InputTextModule,
    ValidationComponentModule,
    InputTextareaModule,
    ButtonModule,
    InputNumberModule,
    FileUploadModule,
    FileSizePipeModule,
    ToastModule,
  ],
  providers: [ImageAdapter],
})
export class CreateAdModule {}
