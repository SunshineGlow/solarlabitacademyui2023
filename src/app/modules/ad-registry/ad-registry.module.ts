import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdRegistryRoutingModule } from './ad-registry-routing.module';
import { RecommendedAdsComponent } from './pages/recommended-ads/recommended-ads.component';
import { AdsFpsComponent } from './pages/ads-fps/ads-fps.component';
import { AdViewComponent } from './pages/ad-view/ad-view.component';
import { CardComponentModule } from '../../shared/components/card/card.component';
import { CurrencyFormatPipeModule } from '../../shared/pipes/currency-format.pipe';
import { ButtonModule } from 'primeng/button';
import { SkeletonModule } from 'primeng/skeleton';
import { GalleryComponentModule } from '../../shared/components/gallery/gallery.component';

@NgModule({
  declarations: [RecommendedAdsComponent, AdsFpsComponent, AdViewComponent],
  imports: [
    CommonModule,
    AdRegistryRoutingModule,
    CardComponentModule,
    CurrencyFormatPipeModule,
    ButtonModule,
    SkeletonModule,
    GalleryComponentModule,
  ],
})
export class AdRegistryModule {}
