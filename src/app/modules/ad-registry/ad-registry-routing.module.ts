import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecommendedAdsComponent } from './pages/recommended-ads/recommended-ads.component';
import { AdsFpsComponent } from './pages/ads-fps/ads-fps.component';
import { AdViewComponent } from './pages/ad-view/ad-view.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: RecommendedAdsComponent,
  },
  {
    path: 'view/:id',
    pathMatch: 'full',
    component: AdViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdRegistryRoutingModule {}
