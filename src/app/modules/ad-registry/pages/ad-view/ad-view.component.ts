import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/dynamicdialog';
import { PhoneModalComponent } from '../../../../shared/modals/phone-modal/phone-modal.component';

@Component({
  selector: 'app-ad-view',
  templateUrl: './ad-view.component.html',
  styleUrls: ['./ad-view.component.scss'],
})
export class AdViewComponent implements OnInit {
  public data: any = false;

  constructor(private readonly dialogService: DialogService) {}

  public ngOnInit() {
    //TODO Убрать таймаут
    setTimeout(() => {
      this.data = {
        images: [
          {
            src: 'https://cc0.photo/wp-content/uploads/2022/03/European-hedgehog-looking-at-camera-980x735.jpg',
          },
          {
            src: 'https://cc0.photo/wp-content/uploads/2022/03/Mossy-stones-in-Bohemian-Paradise-Czech-Republic-980x651.jpg',
          },
          {
            src: 'https://cc0.photo/wp-content/uploads/2022/03/Icelandic-glacier-field-in-fog-980x651.jpg',
          },
          {
            src: 'https://cc0.photo/wp-content/uploads/2022/03/Svartifoss-waterfall-in-Iceland-980x651.jpg',
          },
          {
            src: 'https://cc0.photo/wp-content/uploads/2022/03/Old-glass-balloon-in-front-of-wire-mesh-fence-980x653.jpg',
          },
          {
            src: 'https://cc0.photo/wp-content/uploads/2022/03/Rose-branches-in-the-sun-lantern-in-the-background-980x653.jpg',
          },
        ],
      };
    }, 1000);
  }

  public showPhoneModal(): void {
    // TODO Имя, или если нет - "Пользователь"
    this.dialogService.open(PhoneModalComponent, {
      header: 'Пользователь',
      data: {
        phone: '+79789876543',
      },
      style: {
        width: '516px',
      },
    });
  }
}
