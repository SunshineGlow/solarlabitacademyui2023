import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IPagedState } from '../../../../shared/models/i-paged-state';

@Component({
  selector: 'app-recommended-ads',
  templateUrl: './recommended-ads.component.html',
  styleUrls: ['./recommended-ads.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecommendedAdsComponent implements OnInit {
  //TODO Контракты
  public readonly state$ = new BehaviorSubject<IPagedState<any>>({
    isLoading: true,
    data: [],
  });
  public skeletonArr = new Array(20);

  public ngOnInit(): void {
    //TODO убрать таймаут
    setTimeout(() => {
      this.state$.next({
        isLoading: false,
        data: new Array(20).fill({
          id: 0,
          title: 'Гитара фендер',
          price: 20000,
          imgSrc: 'https://placehold.co/600x400',
          createdAt: 'Сегодня 14:12',
          address: 'Москва',
        }),
      });
    }, 1000);
  }
}
