import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IPagedState } from '../../../../shared/models/i-paged-state';

@Component({
  selector: 'app-my-ads',
  templateUrl: './my-ads.component.html',
  styleUrls: ['./my-ads.component.scss'],
})
export class MyAdsComponent {
  //TODO Контракты
  public readonly state$ = new BehaviorSubject<IPagedState<any>>({
    isLoading: true,
    data: [],
  });
  public skeletonArr = new Array(10);

  public ngOnInit(): void {
    //TODO убрать таймаут
    setTimeout(() => {
      this.state$.next({
        isLoading: false,
        data: new Array(10).fill({
          id: 0,
          title: 'Гитара фендер',
          price: 20000,
          imgSrc: 'https://placehold.co/600x400',
          createdAt: 'Сегодня 14:12',
          address: 'Москва',
        }),
      });
    }, 1000);
  }
}
