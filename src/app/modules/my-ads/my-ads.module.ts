import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyAdsRoutingModule } from './my-ads-routing.module';
import { MyAdsComponent } from './pages/my-ads/my-ads.component';
import { CardComponentModule } from '../../shared/components/card/card.component';
import { NewAdCardComponent } from './components/new-ad-card/new-ad-card.component';

@NgModule({
  declarations: [MyAdsComponent, NewAdCardComponent],
  imports: [CommonModule, MyAdsRoutingModule, CardComponentModule],
})
export class MyAdsModule {}
