import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  /**
   * Используется кастомный markAsDirty, т.к. компонент pInputText реализовывает OnPush ,а для рендера ошибки нужно пометить его как markForCheck. Это делается с помощью updateValueAndValidity
   */
  public markAllAsDirty(form: FormGroup) {
    Object.values(form.controls).forEach((control) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
  }
}
export const NOBRKBL_SEPARATOR = '\u202F';
