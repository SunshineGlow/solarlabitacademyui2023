import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly LOCAL_STORAGE_KEY = 'user';

  //TODO Контракты
  private _user$ = new BehaviorSubject<any | null>(null);

  public user$ = this._user$.asObservable();
  public isAuthenticated$ = this._user$
    .asObservable()
    .pipe(map((user) => !!user));

  constructor(private readonly router: Router) {
    const userFromStorage = localStorage.getItem(this.LOCAL_STORAGE_KEY);
    if (userFromStorage) {
      this._user$.next(JSON.parse(userFromStorage));
    }
  }

  //TODO API
  login(): void {
    this._user$.next({});
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify({ a: 1 }));
  }

  //TODO API
  register(): void {
    this._user$.next({});
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify({ a: 1 }));
  }

  //TODO API
  logout(): void {
    this._user$.next(null);
    localStorage.removeItem(this.LOCAL_STORAGE_KEY);
    this.router.navigateByUrl('/');
  }
}
