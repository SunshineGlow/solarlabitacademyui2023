import { TestBed } from '@angular/core/testing';

import { OnlyAuthorizedGuard } from './only-authorized.guard';

describe('OnlyAuthorizedGuard', () => {
  let guard: OnlyAuthorizedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(OnlyAuthorizedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
