import { Observable } from 'rxjs';

export interface Adapter<To, From> {
  adapt(item: From): To | Observable<To>;
}
