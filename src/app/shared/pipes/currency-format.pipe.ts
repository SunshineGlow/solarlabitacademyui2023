import { NgModule, Pipe, PipeTransform } from '@angular/core';
import * as AutoNumeric from 'autonumeric';
import { NOBRKBL_SEPARATOR } from '../../core/services/utils.service';

@Pipe({
  name: 'currencyFormat',
})
export class CurrencyFormatPipe implements PipeTransform {
  transform(value?: number | null): string | null {
    if (value === null || value === undefined) {
      return null;
    }
    if (!value) {
      return `0 ₽`;
    }
    return `${AutoNumeric.format(value, {
      digitGroupSeparator: NOBRKBL_SEPARATOR + NOBRKBL_SEPARATOR,
      currencySymbol: `${NOBRKBL_SEPARATOR}₽`,
      currencySymbolPlacement: 's',
      maximumValue: '9'.repeat(15),
      allowDecimalPadding: 'floats',
    })}`;
  }
}

@NgModule({
  declarations: [CurrencyFormatPipe],
  exports: [CurrencyFormatPipe],
})
export class CurrencyFormatPipeModule {}
