import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  NgModule,
} from '@angular/core';
import { NgIf } from '@angular/common';
import { SharedModule } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { InputTextModule } from 'primeng/inputtext';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { AuthService } from '../../../core/services/auth.service';
import { UtilsService } from '../../../core/services/utils.service';
import { ValidationComponentModule } from '../../components/validation/validation.component';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginModalComponent {
  get isLogin(): boolean {
    return this._isLogin;
  }

  set isLogin(value: boolean) {
    this._isLogin = value;
    this.config.header = value ? 'Авторизация' : 'Регистрация';
  }
  private _isLogin: boolean = true;
  loginForm: FormGroup = this._fb.group({
    phone: ['', Validators.required],
    password: ['', Validators.required],
  });
  registerForm: FormGroup = this._fb.group({
    phone: ['', Validators.required],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
  });
  constructor(
    public readonly config: DynamicDialogConfig,
    private readonly _dialogRef: DynamicDialogRef,
    private readonly _fb: FormBuilder,
    private readonly _utilsService: UtilsService,
    private readonly _authService: AuthService
  ) {}

  login() {
    this._utilsService.markAllAsDirty(this.loginForm);
    if (this.loginForm.invalid) {
      return;
    }
    this._authService.login();
    this._dialogRef.close(true);
  }

  register() {
    this._utilsService.markAllAsDirty(this.registerForm);
    if (this.registerForm.invalid) {
      return;
    }
    this._authService.register();
    this._dialogRef.close(true);
  }
}

@NgModule({
  declarations: [LoginModalComponent],
  exports: [LoginModalComponent],
  imports: [
    NgIf,
    SharedModule,
    InputTextModule,
    ReactiveFormsModule,
    ButtonModule,
    ValidationComponentModule,
  ],
})
export class LoginModalComponentModule {}
