import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DividerModule } from 'primeng/divider';
import { PhoneFormatPipeModule } from '../../pipes/phone-format.pipe';

@Component({
  selector: 'app-phone-modal',
  templateUrl: './phone-modal.component.html',
  styleUrls: ['./phone-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PhoneModalComponent {
  public get phone(): string {
    return this.config.data.phone;
  }
  constructor(
    public readonly config: DynamicDialogConfig,
    private readonly _dialogRef: DynamicDialogRef
  ) {}
}

@NgModule({
  declarations: [PhoneModalComponent],
  exports: [PhoneModalComponent],
  imports: [DividerModule, PhoneFormatPipeModule],
})
export class PhoneModalComponentModule {}
