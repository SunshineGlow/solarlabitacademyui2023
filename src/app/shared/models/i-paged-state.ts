export interface IPagedState<T> {
  data: T[];
  isLoading: boolean;
}
