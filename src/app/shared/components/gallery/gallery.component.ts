import { Component, Input, NgModule } from '@angular/core';
import { SkeletonModule } from 'primeng/skeleton';
import { NgForOf, NgIf } from '@angular/common';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent {
  @Input() public data: any;
  public selectedIndex: number = 0;
}

@NgModule({
  declarations: [GalleryComponent],
  exports: [GalleryComponent],
  imports: [SkeletonModule, NgIf, NgForOf],
})
export class GalleryComponentModule {}
