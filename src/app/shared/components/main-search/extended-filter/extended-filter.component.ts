import { Component, Input } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-extended-filter',
  templateUrl: './extended-filter.component.html',
  styleUrls: ['./extended-filter.component.scss'],
  animations: [
    trigger('filterAnimation', [
      transition(':enter', [
        style({ height: '1px', opacity: 0 }),
        animate('300ms', style({ height: '850px', opacity: 1 })),
      ]),
      transition(':leave', [
        animate('300ms', style({ height: '1px', opacity: 0 })),
      ]),
    ]),
  ],
})
export class ExtendedFilterComponent {
  @Input() public isFilterOpened: boolean = false;
}
