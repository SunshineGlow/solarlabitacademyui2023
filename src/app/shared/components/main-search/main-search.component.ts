import { Component, NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { NgIf } from '@angular/common';
import { ExtendedFilterComponent } from './extended-filter/extended-filter.component';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-main-search',
  templateUrl: './main-search.component.html',
  styleUrls: ['./main-search.component.scss'],
})
export class MainSearchComponent {
  public isFilterOpened = false;

  public toggleFilter(): void {
    this.isFilterOpened = !this.isFilterOpened;
  }
}

@NgModule({
  declarations: [MainSearchComponent, ExtendedFilterComponent],
  exports: [MainSearchComponent],
  imports: [ButtonModule, InputTextModule, NgIf, RouterLink],
})
export class MainSearchComponentModule {}
