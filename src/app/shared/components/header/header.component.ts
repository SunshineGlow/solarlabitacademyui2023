import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { AuthService } from '../../../core/services/auth.service';
import { AsyncPipe, NgIf } from '@angular/common';
import { MenuModule } from 'primeng/menu';
import { MenuItem } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import {
  LoginModalComponent,
  LoginModalComponentModule,
} from '../../modals/login-modal/login-modal.component';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  public readonly menuItems: MenuItem[] = [
    {
      label: 'Мои объявления',
      command: () => this.router.navigateByUrl('/my-ads'),
    },
    { label: 'Настройки' },
    { label: 'Выйти', command: () => this.authService.logout() },
  ];

  constructor(
    public readonly authService: AuthService,
    private readonly dialogService: DialogService,
    private readonly router: Router
  ) {}

  showLoginDialog() {
    this.dialogService.open(LoginModalComponent, {
      header: 'Авторизация',
      style: {
        width: '416px',
      },
    });
  }
}

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    ButtonModule,
    AsyncPipe,
    NgIf,
    MenuModule,
    DialogModule,
    LoginModalComponentModule,
  ],
  exports: [HeaderComponent],
  providers: [DialogService],
})
export class HeaderComponentModule {}
