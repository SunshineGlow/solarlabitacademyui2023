import {
  ChangeDetectionStrategy,
  Component,
  Input,
  NgModule,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { NgIf, NgSwitch, NgSwitchCase } from '@angular/common';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss'],
  animations: [
    trigger('validationAnimation', [
      transition(':enter', [
        style({ height: '1px', opacity: 0 }),
        animate('100ms ease-out', style({ height: '16px', opacity: 1 })),
      ]),
      transition(':leave', [
        animate('100ms ease-out', style({ height: '1px', opacity: 0 })),
      ]),
    ]),
  ],
})
export class ValidationComponent {
  @Input() public control: AbstractControl | null = null;
}

@NgModule({
  declarations: [ValidationComponent],
  exports: [ValidationComponent],
  imports: [NgSwitch, NgIf, NgSwitchCase],
})
export class ValidationComponentModule {}
