import {
  ChangeDetectionStrategy,
  Component,
  Input,
  NgModule,
} from '@angular/core';
import { NgIf } from '@angular/common';
import { RouterLink } from '@angular/router';
import { CurrencyFormatPipeModule } from '../../pipes/currency-format.pipe';
import { SkeletonModule } from 'primeng/skeleton';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {
  // TODO Контракты
  @Input() public ad: any | null = null;
}

@NgModule({
  declarations: [CardComponent],
  exports: [CardComponent],
  imports: [NgIf, RouterLink, CurrencyFormatPipeModule, SkeletonModule],
})
export class CardComponentModule {}
