import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizedGuard } from './core/guards/authorized.guard';
import { RecommendedAdsComponent } from './modules/ad-registry/pages/recommended-ads/recommended-ads.component';
import { OnlyAuthorizedGuard } from './core/guards/only-authorized.guard';

const routes: Routes = [
  {
    path: 'my-ads',
    loadChildren: () =>
      import('./modules/my-ads/my-ads.module').then((m) => m.MyAdsModule),
    canLoad: [OnlyAuthorizedGuard],
  },
  {
    path: 'create-ad',
    loadChildren: () =>
      import('./modules/create-ad/create-ad.module').then(
        (m) => m.CreateAdModule
      ),
    canLoad: [OnlyAuthorizedGuard],
  },
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () =>
      import('./modules/ad-registry/ad-registry.module').then(
        (m) => m.AdRegistryModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
