export interface EnvironmentModel {
  isProduction: boolean;
  apiServer: string;
}
