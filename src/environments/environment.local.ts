import { EnvironmentModel } from './models/environment.model';

export const environment: EnvironmentModel = {
  isProduction: true,
  apiServer: 'http://localhost:5000',
};
