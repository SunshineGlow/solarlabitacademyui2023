import { EnvironmentModel } from './models/environment.model';

export const environment: EnvironmentModel = {
  isProduction: true,
  apiServer: 'http://80.90.184.170:5000',
};
